
import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';

class qrGenerate extends StatefulWidget {
double? latitude;
double? longitude;
   qrGenerate({super.key, required this.latitude, required this.longitude});

  @override
  State<qrGenerate> createState() => _qrGenerateState();
}

class _qrGenerateState extends State<qrGenerate> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:Center(
        child: QrImageView(
                data: '${widget.latitude} ${widget.longitude}',
                version: QrVersions.auto,
                size: 200.0,
              ),
      ) ,
    );
  }
}