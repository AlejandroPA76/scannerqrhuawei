import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:huawei_scan/huawei_scan.dart';
import 'package:flutter/material.dart';
import 'package:new_gps_camera/blocs/gps/gps_bloc.dart';
import 'package:new_gps_camera/screens/screens.dart';
class scanner extends StatefulWidget {
  const scanner({super.key});

  @override
  State<scanner> createState() => _scannerState();
}

class _scannerState extends State<scanner> {
  
  @override
  Widget build(BuildContext context) {
    return Center(
    child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        ElevatedButton(onPressed: () async{
            DefaultViewRequest request = new DefaultViewRequest(
    scanType: HmsScanTypes.AllScanType,
    viewType: 1,
    errorCheck: true,
  );
         await HmsScanUtils.startDefaultView(request);
        }, child: Text("scanner")),

        ElevatedButton(onPressed: () async{
      
           
          CustomizedViewRequest request = new CustomizedViewRequest(
            
            scanType: HmsScanTypes.QRCode,
            additionalScanTypes: [HmsScanTypes.Aztec, HmsScanTypes.DataMatrix],
            rectHeight: 240,
            rectWidth: 240,
            isFlashAvailable: false,
            isGalleryAvailable: false,
            continuouslyScan: false,
            
            customizedCameraListener: (ScanResponse response) {
             // debugPrint(response.showResult);        
              print("el Qr es: ${response.showResult}");
            },
           
          );
           await HmsCustomizedView.startCustomizedView(request);
        
        }, child: Text("personalizado")),

        ElevatedButton(onPressed: () {
          Navigator.push(context, MaterialPageRoute(builder: (context) => GpsAccessScreen(),));         
        }, child: Text("menus"))
      ],
    )
    );
  }
}