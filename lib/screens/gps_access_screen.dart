
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:new_gps_camera/blocs/gps/gps_bloc.dart';

class GpsAccessScreen extends StatelessWidget {
  const GpsAccessScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: 
        BlocBuilder<GpsBloc, GpsState>(
          builder: (context, state) {
            print("el estado de cada variable: ${state}");

          return !state.isGpsEnabled
          ? const _EnableGpsMessage()
          : const _AccessButton();
        },)
        //  _EnableGpsMessage()
        //_AccessButton(),
      ),
    );
  }
}

class _AccessButton extends StatelessWidget {
  const _AccessButton({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text("El GPS esta activado"),
        MaterialButton(
          child: Text("Aceptar",
          style: TextStyle(color: Colors.white),
          
          
          ),
          color: Colors.black,
          shape: const StadiumBorder(),
          elevation: 0,
          splashColor: Colors.transparent,
          onPressed: () {
          //TODO:  por hacer
          final gpsBloc = BlocProvider.of<GpsBloc>(context);
          gpsBloc.askGpsAccess();
        },)
      ],
    );
  }
}

class _EnableGpsMessage extends StatelessWidget {
  const _EnableGpsMessage({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return const Text("Debe de habilitar el GPS",
    style: TextStyle(fontSize: 25, fontWeight: FontWeight.w300),
    );
  }
}